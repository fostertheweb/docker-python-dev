# docker-python-dev
Simple development environment for python with tmux, vim, and my dot files. Not very useful for anyone else but you can easily just fork this repo and edit the Dockerfile to add what you need for development. I just wanted a very basic and quick python environment because I've always struggled to get a working local python development environment with the version I want. Also fun to learn more about Docker.

### Clone this repo and build the image
```
docker build -t python-dev .
```

### Want to push to GitHub?
When executing `docker run` you can mount a data volume to include your SSH key in the container
```
docker run -it -v ~/.ssh/id_rsa:/root/.ssh/id_rsa:ro --name pydev python-dev
```

### Pull the image from [Docker Hub](https://hub.docker.com)
The image is built for me with my dot files and preferred packages ready to go, so I can simply pull the image from Docker Hub
```
docker pull fostertheweb/python-dev
```
and to run it
```
docker run -it -v ~/.ssh/id_rsa:/root/.ssh/id_rsa:ro --name pydev fostertheweb/python-dev
```

